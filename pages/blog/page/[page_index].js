import Layout from "@/components/Layout"
import Post from "@/components/Post"
import CategoryList from "@/components/CategoryList"
import Pagination from "@/components/Pagination"
import {POST_PER_PAGE} from '@/config/index'
import { getPosts } from "@/lib/posts"
import path from "path"
import fs from "fs"


export default function BlogPage({posts, numPages, currentPage, categories}) {
  return (
    <Layout title="Dev Space" description="i" keywords="p" >
      <div className="flex justify-between">
        <div className="w-3/4 mr-10">
          <h1 className="text-5xl border-b-4 p-5 font-bold">Blogs</h1>
          <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-5">
            {posts.map((post, index) => (
              <Post key={index} post={post} />
            ))}
          </div>
        </div>
        
        <div className="w-1/4">
          <CategoryList categories={categories} />
        </div>
      </div>

      <Pagination currentPage={currentPage} numPages={numPages} />
    </Layout>
  )
}

export async function getStaticPaths() {
    const files = fs.readdirSync(path.join('posts'))
    const numPages = Math.ceil(files.length / POST_PER_PAGE)

    let paths = []

    for (let index = 1; index <= numPages; index++) {
        paths.push({
            params : {page_index : index.toString()}
        })
    }

    return {
        paths,
        fallback : false
    }
}

export async function getStaticProps({params}) {
    const page = parseInt((params && params.page_index) || 1 )
    const files = fs.readdirSync(path.join('posts'))

    const posts = getPosts()

    const categories = posts.map((post) => post.frontmatter.category)
    // mencari unique dalam kategoris dalam artian menghapus item yang sama
    const uniqueCategories = [...new Set(categories)]
    console.log(uniqueCategories)

    const numPages = Math.ceil(files.length / POST_PER_PAGE)
    const pageIndex = page - 1
    const orderedPosts = posts.slice(pageIndex * POST_PER_PAGE, (pageIndex + 1)* POST_PER_PAGE)
    // console.log(posts)
    return {
        props : {
            posts : orderedPosts,
            numPages,
            currentPage : page,
            categories : uniqueCategories

        },
    }
}
