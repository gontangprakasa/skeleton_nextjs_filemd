import Link from 'next/link'
import { Children } from 'react'

export default function CategoryLabel({children}) {
    const ColorKey = {
        JavaScript :'yellow',
        CSS : 'blue',
        Python : 'green',
        PHP : 'purple',
        Ruby : 'red'
    }

    return (
        <div className={`px-2 py-1 bg-${ColorKey[children]}-600 text-gray-100 font-bold rounded`}>
            <Link href={`/blog/category/${children.toLowerCase()}`}>
                {children}
            </Link>
        </div>
    )
}
